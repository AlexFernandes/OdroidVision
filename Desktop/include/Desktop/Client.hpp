 /***
  Author:       Alexander Fernandes
  Created:      March 10, 2018
  Last updated: March 20, 2018

  Description:
    This class handles receiving video frames and displaying them over TCP/IP connection to a server
 ***/

// Include a guard to avoid similar header file names
#ifndef __CLIENT_HPP_INCLUDED__
#define __CLIENT_HPP_INCLUDED__

// Core C++ Headers
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

// 3rd Party Headers
#include "opencv2/opencv.hpp"

// Project Headers

class Client
{

  public:

    /*** Constructor ***/
    Client(std::string _serverIP, int _port = 6969);
    /* Input:
      _serverIP: server IP address
      _port: connection port
    */

    /*** Deconstructor ***/
    ~Client();

    /*** Public Methods ***/

    int connect_server(void);
    /* Connect to listening server
      Input:
        -void-
      Output:
        return: If the connection or binding succeeds, zero is returned. On error, -1 is returned, and errno is set appropriately.
    */

    cv::Mat recv_frame(void);
    /* Receive encoded video frame and return it as a OpenCV Mat variable
      Input:
        -void-
      Output:
        return: cv::Mat OpenCV frame
    */

    cv::Mat recv_compressed_frame(void);
    /* Receive compressed video frame and return the cv::imdecode() video frame
      Input:
        -void-
      Output:
        return: cv::Mat OpenCV frame
    */

    /*** Public Variables ***/

  private:

    /*** Private Variables ***/

    short int p_imageWidth; // video frame width dimension
    short int p_imageHeight; // video frame height dimension

    int p_socket; // connection socket
    int p_serverPort; // server port
    int p_addressLength; // length of sockaddr_in

    bool p_serverConnected; // check if connected to a server

    struct sockaddr_in p_serverAddress;

};

#endif
