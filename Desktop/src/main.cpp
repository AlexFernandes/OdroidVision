/***
  Author:       Alexander Fernandes
  Created:      March 13, 2018
  Last updated: March 13, 2018

  Description:
    Example use of OdroidVision API project
 ***/

// C++ Core libraries

// 3rd Party Headers
#include "opencv2/opencv.hpp"

// Project Headers
#include "Desktop/Client.hpp"

using namespace std;

int main(int argc, char** argv) {

  string  ipAddress = "";
  string  windowName = "iTAD View";
  int     delay = 3; // ms

  cout << "Enter Odroid Vision IP Address: ";
  cin >> ipAddress;

  Client odroidVision(ipAddress);

  cv::namedWindow(windowName, 0);

  if (odroidVision.connect_server() == 0) {

    std::cout << "Connected to Odroid Vision!" << std::endl;

    while(cv::waitKey(delay)) {
      // Receive uncompressed video frame
      // cv::imshow(windowName, odroidVision.recv_frame());

      // Receive compressed video frame
      cv::imshow(windowName, odroidVision.recv_compressed_frame());
    }

  }
}
