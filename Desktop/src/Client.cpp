/***
  Author:       Alexander Fernandes
  Created:      March 10, 2018
  Last updated: March 20, 2018
 ***/

#include "Desktop/Client.hpp"

/* Constructor */

Client::Client(std::string _serverIP, int _port)
{

  // Setup socket
  p_addressLength = sizeof(struct sockaddr_in);
  p_socket = socket(PF_INET, SOCK_STREAM, 0);
  if (p_socket == -1)
    perror("Client socket call failed!");

  // Initialize socket address
  p_serverAddress.sin_family = PF_INET;
  p_serverAddress.sin_addr.s_addr = inet_addr(_serverIP.c_str());
  p_serverAddress.sin_port = htons( _port );

  p_serverConnected = false;

}

Client::~Client() {
  if (p_serverConnected)
    close(p_socket);
}

/* Public Methods */

int Client::connect_server() {

  int l_conRet = connect(p_socket, (sockaddr *)&p_serverAddress, p_addressLength);

  if (l_conRet == 0) {

    // Read video frame dimensions
    recv(p_socket, &p_imageWidth, sizeof(p_imageWidth), 0);
    recv(p_socket, &p_imageHeight, sizeof(p_imageHeight), 0);

    std::cout << "Received Video Width: " << p_imageWidth << std::endl;
    std::cout << "Received Video Height: " << p_imageHeight << std::endl;
  }

  return l_conRet;
}

cv::Mat Client::recv_frame(void) {

  cv::Mat l_img;
  // For RGB: CV_8UC3 for 8-bit * 3 rgb
  l_img = cv::Mat::zeros(p_imageHeight, p_imageWidth, CV_8UC3);

  // For Grayscale: CV_8UC1 8-bit * 1 grayscale
  // l_img = cv::Mat::zeros(p_imageHeight, p_imageWidth, CV_8UC1);

  int l_imgSize = l_img.total() * l_img.elemSize();
  uchar *l_iptr = l_img.data;

  // Receive image
  recv(p_socket, l_iptr, l_imgSize, MSG_WAITALL);

  return l_img;

}

cv::Mat Client::recv_compressed_frame(void) {

  cv::Mat       l_img;
  unsigned int  l_bufSize;

  // Receive bufSize
  recv(p_socket, &l_bufSize, sizeof(l_bufSize), MSG_WAITALL);

  uchar l_buf[l_bufSize];

  // Receive image
  recv(p_socket, l_buf, l_bufSize, MSG_WAITALL);

  std::vector<uchar>  l_bufVector(l_buf, l_buf + l_bufSize);

  l_img = cv::imdecode(l_bufVector, CV_LOAD_IMAGE_COLOR);

  return l_img;

}
