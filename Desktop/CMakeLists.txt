cmake_minimum_required( VERSION 2.8 )

# Steps to follow:
# 1. Set the project name which will also be the binary file name
# 2. Add all class source files under src/ directory
# 3. Add all corresponding class header files under include/ directory

# 1. Set project name
set( PROJECT_NAME

    ##### Edit below this line #####

    # Ensure there are no spaces in between quotes: ""
    "Desktop"

    ##### Edit above this line #####
)

PROJECT ( ${PROJECT_NAME} )
find_package( OpenCV REQUIRED )

# 2. Add src files
set( NAME_SRC
    src/main.cpp # Do not remove

    ##### Edit below this line #####

    src/Client.cpp

    ##### Edit above this line #####

)

# 3. Add header files
set( NAME_HEADERS

    ##### Edit below this line #####

    include/Desktop/Client.hpp

    ##### Edit above this line #####

)

INCLUDE_DIRECTORIES( ${CMAKE_CURRENT_SOURCE_DIR}/include )
link_directories( ${CMAKE_BINARY_DIR}/bin)
set( EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin )
add_executable( ${PROJECT_NAME} ${NAME_SRC} ${NAME_HEADERS} )
target_link_libraries( ${PROJECT_NAME} ${OpenCV_LIBS} )
