/***
  Author:       Alexander Fernandes
  Created:      March 5, 2018
  Last updated: March 21, 2018

  Description:
    The main class to control all computer vision aspects on the Odroid
 ***/

// Include a guard to avoid similar header file names
#ifndef __ODROIDVISION_HPP_INCLUDED__
#define __ODROIDVISION_HPP_INCLUDED__

// Core C++ Headers

// 3rd Party Headers

// Project Headers
#include "OdroidStreamer/CascadeDetector.hpp"
#include "OdroidStreamer/Recognizer.hpp"
#include "OdroidStreamer/Server.hpp"

class OdroidVision
{

  public:

    /*** Constructor ***/
    OdroidVision(const std::string _frontalFaceCascadeClassifier = "./data/cascades/haarcascade_frontalface_default.xml",
                 int               _cameraIndex = 0,
                 const std::string _userDir = "./data/Users/",
                 const std::string _eigenFaceXml = "./data/Users/EigenFace.xml");
    /* Input:
        _frontalFaceCascadeClassifier: Haar cascade classifier for basic frontal face detection
        _cameraIndex: specifies desired camera index if there are multiple cameras
    */

    ~OdroidVision();

    /*** Public Methods ***/

    bool update(int _detectOnFrame);
    /* Update all computer vision processes on a single image frame. This function should be called always at the start of a loop

      1. capture frame
      2. create a copy of the captured frame for the draw frame
      3. detect all frontal faces
      4. find largest face (to track)
      5. calculate arrow vector: center of screen to center of largest face
      6. perform face recognition using Recognizer class

      Input:
        _detectOnFrame: perform the frontal face detection on every "_detectOnFrame"
                        but still capture video frames on every frame. If set to 1, 0
                        or negative number, then it will perform detection on every frame
      Output:
        return: if captured frame is not empty it will return true otherwise false
    */

    void draw_detected_faces(cv::Scalar _colour = cv::Scalar( 0, 255, 255 ),
                             int        _thickness = 2,
                             int        _lineType = 8);
    /* Draw the detected faces onto drawFrame
      Input:
        _colour: Rectangle color or brightness (grayscale image). cv::Scalar( Blue, Green, Red )
        _thickness: Thickness of lines that make up the rectangle. Negative values, like CV_FILLED , mean that the function has to draw a filled rectangle.
        _lineType: Type of the line. See the cv::line() description.
      Output:
        -void-
    */

    void draw_arrow(cv::Scalar  _colour = cv::Scalar( 0, 0, 255 ),
                    int         _thickness = 3);
    /* Draw the detected faces onto drawFrame
      Input:
        _colour: Rectangle color or brightness (grayscale image). cv::Scalar( Blue, Green, Red )
        _thickness: Thickness of lines that make up the rectangle. Negative values, like CV_FILLED , mean that the function has to draw a filled rectangle.
      Output:
        -void-
    */

    void draw_predictedFaceLabel(int        _fontFace = cv::FONT_HERSHEY_SIMPLEX,
                                 double     _fontScale = 1,
                                 cv::Scalar _colour = cv::Scalar( 0, 0, 255, 255 ),
                                 int        _thickness = 2,
                                 int        _lineType = 8,
                                 bool       _bottomLeftOrigin = false);
    /* Draw the detected faces onto drawFrame
      Input:
        _fontFace: Font type. One of FONT_HERSHEY_SIMPLEX, FONT_HERSHEY_PLAIN,
                  FONT_HERSHEY_DUPLEX, FONT_HERSHEY_COMPLEX, FONT_HERSHEY_TRIPLEX,
                  FONT_HERSHEY_COMPLEX_SMALL, FONT_HERSHEY_SCRIPT_SIMPLEX, or
                  FONT_HERSHEY_SCRIPT_COMPLEX, where each of the font ID’s can be
                  combined with FONT_ITALIC to get the slanted letters.
        _fontScale: Font scale factor that is multiplied by the font-specific base size.
        _color: Text color.
        _thickness: Thickness of the lines used to draw a text.
        _lineType: Line type. See the line for details.
        _bottomLeftOrigin: When true, the image data origin is at the bottom-left corner. Otherwise, it is at the top-left corner.
       Output:
        -void-
    */

    void send_videoFrame(cv::Mat _sendFrame);
    /* Send OpenCV frame over TCP/IP using Server class
      Input:
        _sendFrame: the OpenCV frame to send
      Output:
        -void-
    */

    void display_capturedFrame(std::string _title,
                               int         _delay = 10);
    /* Display window based on title
      Input:
        _title: window name
        _delay: delay time in milliseconds to update display window. 0 is never update screen
    */

    void display_drawFrame(std::string _title,
                           int         _delay = 10);
    /* Display window based on title
      Input:
        _title: window name
        _delay: delay time in milliseconds to update display window. 0 is never update screen
    */

    /*** Public Variables ***/

    std::vector<cv::Rect> detectedFaces;    // list of all detected frontal faces
    std::string           predictedFaceUserName;    // name of user with highest predicted face
    cv::Mat               capturedFrame;    // image frame captured by camera (this image is never modified)
    cv::Mat               drawFrame;        // image frame used to draw on
    cv::Rect              screenDimensions; // screen dimensions rectangle
    cv::Rect              largestFaceRect;  // location for largest face as a rectangle
    cv::Point             centerScreen;     // center of screen
    cv::Point             largestFacePoint; // center point of the largest face
    cv::Point             arrow;            // center of screen to center of largest face detected

  private:

    /*** Private Methods ***/

    bool capture_frame(void);
    /* Capture video feed frame into capturedFrame variable and screen dimensions into screenDimensions.
      Input:
        -void-
      Output:
        return: if captured frame is not empty it will return true otherwise false
    */

    cv::Point calculate_center(cv::Rect _rectangle);
    /* Calculates center point of any rectangle
      Input:
        _rectangle: input rectangle
      Output:
         return: center point
    */

    /*** Private Variables ***/

    cv::VideoCapture  p_camera; // web camera to capture image frames
    CascadeDetector   p_faceClassifier; // classifier for face detection
    Recognizer        p_userRecognizer; // perform face recognition of users
    Server            p_server; // server class to send video frames over TCP/IP
    int               p_detectOnFrameCounter; // used as a counter for performing on other frame detection in update()
};

#endif
