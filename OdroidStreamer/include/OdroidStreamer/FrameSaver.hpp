 /***
  Author:       Alexander Fernandes
  Created:      March 8, 2018
  Last updated: March 8, 2018

  Description:
    This class handles image/video-frame saving and managing files/folders
 ***/

// Include a guard to avoid similar header file names
#ifndef __FRAMESAVER_HPP_INCLUDED__
#define __FRAMESAVER_HPP_INCLUDED__

// Core C++ Headers
#include <sys/stat.h> // mkdir directory
#include <fstream> // file stream

// 3rd Party Headers
#include "opencv2/opencv.hpp"

// Project Headers
#include "OdroidStreamer/CascadeDetector.hpp"

class FrameSaver
{

  public:

    /*** Constructor ***/

    FrameSaver(std::string _cropCascadeFile = "",
               std::string _dataDir = "./data/");
    /* Input:
        _cropCascadeFile: cascade file for optional cropping, the cascade.xml should
                          be located in cascades/ directory (default = "" for no cropping)
        _dataDir: top directory to save all images under
    */

    /*** Public Methods ***/

    void save_frame(cv::Mat     _frame,
                    std::string _folderName,
                    std::string _fileName);
    /* Save an OpenCV Mat frame as an image under a specified directory with specified name.
       The directory will be made if it does not exist
      Input:
        _frame: the opencv Mat type matrix variable to save into an image
        _folderName: name of directory in data/ to store saved images
        _fileName: name of file (specify as .png or .jpg for this to work!)
      Output:
        -void-
    */

    void save_largest_crop_frame(cv::Mat     _frame,
                                 std::string _folderName,
                                 std::string _fileName);
    /* Save an OpenCV Mat frame and crop to the largest object detected using CascadeDetector p_cropCascade
      Input:
        _frame: the opencv Mat type matrix variable to save into an image
        _folderName: name of directory in data/ to store saved images
        _fileName: name of file (specify as .png or .jpg for this to work!)
      Output:
        -void-s
    */

    /*** Public Variables ***/

  private:

    /*** Private Variables ***/
    const std::string p_dataDir; // top level directory (default "./data/")
    CascadeDetector   p_cropCascade; // Cascade Classifier to perform optional cropping

};

#endif
