/***
  Author:       Alexander Fernandes
  Created:      February 6, 2018
  Last updated: March 8, 2018

  Description:
    This class is responsible for handling all OpenCV (face) recognition
 ***/
// Include a guard to avoid similar header file names
#ifndef __RECOGNIZER_HPP_INCLUDED__
#define __RECOGNIZER_HPP_INCLUDED__

// Core C++ Headers
#include <stdlib.h>
#include <dirent.h> // read files in directory

// 3rd Party Headers

#include "opencv2/opencv.hpp"
#include <opencv2/core/ocl.hpp> // OpenCL - using a GPU
#include "opencv2/face.hpp"

class Recognizer
{
  public:

    /*** Constructor ***/
    Recognizer(const std::string _userDir = "./data/Users/",
               const std::string _eigenFaceXml = "./data/Users/EigenFace.xml");
    /* Load trained data
      Input:
         _userDir: path to Users directory
         _eigenFaceXml: trained EigenFace.xml
    */

    /*** Public Methods ***/

    int iterative_recognition(cv::Mat _face);
    /* Perform recognition by iterating through each user after everytime function called
      Input:
         _face: image that contains the region of interest (cropped to face)
      Output:
         return: int label corresponding to userList
    */

    int eigenface_recognition(cv::Mat _face,
                              double  &_confidence,
                              double  _confidenceThreshold);
    /* Perform recognition with EigenFace as prediction
      Input:
         _face: image that contains the region of interest (cropped to face)
         _confidenceThreshold: threshold of recognition from eigenface
      Output:
         return: int label corresponding to userList
    */

    std::string get_username(int _index);
    /* Return username as string using index
      Input:
         _index - label index output by recognition Methods
      Output:
         return - string username or "Unrecognized" if index == -1
    */

    /*** Public Variables ***/

  private:

    /*** Private Methods ***/

    /*** Private Variables ***/

    int                                 p_totalUsers;
    int                                 p_iterUser;
    std::vector<std::string>            p_userList;
    cv::Ptr<cv::face::FaceRecognizer>   p_eigenface;
    std::vector<cv::CascadeClassifier>  p_userClassifierList;
};

#endif
