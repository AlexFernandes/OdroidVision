 /***
  Author:       Alexander Fernandes
  Created:      March 10, 2018
  Last updated: March 20, 2018

  Description:
    This class handles streaming opencv frames over a TCP/IP connection
 ***/

// Include a guard to avoid similar header file names
#ifndef __SERVER_HPP_INCLUDED__
#define __SERVER_HPP_INCLUDED__

// Core C++ Headers
#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <string.h>

// 3rd Party Headers
#include "opencv2/opencv.hpp"

// Project Headers


class Server
{

  public:

    /*** Constructor ***/
    Server(int _port = 6969);
    /* Input:
      _port: connection port
    */

    /*** Deconstructor ***/
    ~Server();

    /*** Public Methods ***/

    void connect(unsigned short _imageWidth, unsigned short _imageHeight, int _backlog = 3);
    /* Listen and wait for a client connection
      Input:
        _imageWidth: 2-byte (0-65,535) width dimension of video frame being sent
        _imageHeight: 2-byte (0-65,535) height dimension of video frame being sent
        _backlog: defines the maximum length to which the queue of
                  pending connections for sockfd may grow. More information
                  about TCP/IP C++ connection can be found here: http://man7.org/linux/man-pages/man2/listen.2.html
      Output:
        -void-
    */

    ssize_t send_frame(cv::Mat _frame);
    /* Sends OpenCV Matrix frame as bytes (char); reference: https://linux.die.net/man/2/send
      Input:
        _frame: the frame to encode and send
      Output:
        return: On success, these calls return the number of bytes sent. On error, -1 is returned, and errno is set appropriately.
    */

    void send_compressed_frame(cv::Mat _frame);
    /* Compresses image using cv::imencode as jpg and sends compressed video frame bytes over TCP/IP
      Input:
        _frame: the frame to encode and send
      Output:
        -void-
    */

    /*** Public Variables ***/

  private:

    /*** Private Variables ***/

    short int p_imageWidth; // width of video frame to send over TCP/IP (number between 0 through 65,535 or 2^16 - 1)
    short int p_imageHeight; // height of video frame to send over TCP/IP (number between 0 through 65,535 or 2^16 - 1)

    int p_localSocket; // server socket
    int p_clientSocket; // client socket
    int p_addressLength; // length of sockaddr_in

    bool p_clientConnected; // check if connected to a client

    struct sockaddr_in  p_localAddress; // server socket address
    struct sockaddr_in  p_clientAddress; // client socket address

};

#endif
