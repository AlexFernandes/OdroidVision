 /***
  Author:       Alexander Fernandes
  Created:      March 5, 2018
  Last updated: March 5, 2018

  Description:
    This class performs the main face detection using the GPU
 ***/

// Include a guard to avoid similar header file names
#ifndef __CASCADEDETECTOR_HPP_INCLUDED__
#define __CASCADEDETECTOR_HPP_INCLUDED__

// Core C++ Headers

// 3rd Party Headers
#include "opencv2/opencv.hpp" // OpenCV
#include <opencv2/core/ocl.hpp> // OpenCL - using a GPU

// Project Headers

class CascadeDetector
{

  public:

    /*** Constructor ***/

    CascadeDetector(const std::string _classifierFile);
    /* Input:
      _classifierFile: location of .xml file to perform cascade detection
    */

    /*** Public Methods ***/

    void detect_objects_multiscale(cv::Mat                _inputFrame,
                                   std::vector <cv::Rect> &_objects,
                                   double                 _scaleFactor = 1.1,
                                   int                    _minNeighbours = 3,
                                   int                    _flags = 0,
                                   cv::Size               _minSize = cv::Size(),
                                   cv::Size               _maxSize = cv::Size());
    /* Performs OpenCV cv::CascadeClassifier::detectMultiScale using the GPU, then overlays detected objects onto detectionFrame
      Input:
        _inputFrame: Matrix copied to GPU of the type CV_8U containing an image where objects are detected.
        _objects: Vector of rectangles where each rectangle contains the detected object.
        _scaleFactor: Parameter specifying how much the image size is reduced at each image scale.
        _minNeighbors: Parameter specifying how many neighbors each candidate rectangle should have to retain it.
        _flags: Parameter with the same meaning for an old cascade as in the function cvHaarDetectObjects. It is not used for a new cascade.
        _minSize: Minimum possible object size. Objects smaller than that are ignored.
        _maxSize: Maximum possible object size. Objects larger than that are ignored.
      Output:
        -void-
    */

    /*** Public Variables ***/

    void draw_detected_objects(cv::Mat    _drawFrame,
                               cv::Scalar _colour = cv::Scalar( 0, 255, 255 ),
                               int        _thickness = 2,
                               int        _lineType = 8,
                               int        _shift = 0);
    /* Draw the detected objects from detect_objects_multiscale onto the _drawFrame argument variable
      Input:
        _drawFrame: the image frame to draw objects onto
        _colour: Rectangle color or brightness (grayscale image). cv::Scalar( Blue, Green, Red )
        _thickness: Thickness of lines that make up the rectangle. Negative values, like CV_FILLED , mean that the function has to draw a filled rectangle.
        _lineType: Type of the line. See the cv::line() description.
        _shift: Number of fractional bits in the point coordinates.
      Output:
        -void-
    */

    bool check_load_error(void);
    /* True only if CascadeClassifier loaded properly
      Input:
        -void
      Output:
        return: p_loadError variable
    */

  private:

    /*** Private Variables ***/

    cv::CascadeClassifier   p_classifier; // OpenCV cascade classifier
    std::vector <cv::Rect>  p_objects; // detected objects
    bool                    p_loadError; // true if CascadeClassifier loaded properly

};

#endif
