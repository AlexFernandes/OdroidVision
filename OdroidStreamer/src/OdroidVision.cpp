/***
  Author:       Alexander Fernandes
  Created:      March 5, 2018
  Last updated: March 21, 2018
 ***/

#include "OdroidStreamer/OdroidVision.hpp"

/* Constructor */

OdroidVision::OdroidVision(
  const std::string _frontalFaceCascadeClassifier,
  int               _cameraIndex,
  const std::string _userDir,
  const std::string _eigenFaceXml
) : // Pass arguments to the private variables:
  p_faceClassifier(_frontalFaceCascadeClassifier),
  p_server(),
  p_userRecognizer(_userDir, _eigenFaceXml)
{
  if (!p_camera.open(_cameraIndex)) {
    perror("Error! Camera failed to open!");
    exit(-1);
  }

  // Initialize rectangles to zero size
  int l_x = 0, l_y = 0, l_width = 0, l_height = 0;
  screenDimensions = cv::Rect(l_x, l_y, l_width, l_height);
  largestFaceRect = cv::Rect(l_x, l_y, l_width, l_height);
  predictedFaceUserName = "";

  if (capture_frame()) {
    // capture a frame to get video dimensions

    // Connect server to local client
    // TODO: add versitality so that program doesn't have to wait for connection
    p_server.connect(screenDimensions.width, screenDimensions.height);

  }

  p_detectOnFrameCounter = 0;

}

/* Deconstructor */

OdroidVision::~OdroidVision() {
  if (p_camera.isOpened())
    p_camera.release();
}

/* Public Methods */

bool OdroidVision::update(int _detectOnFrame) {

  if (_detectOnFrame < 1)
    _detectOnFrame = 1;

  // 1. capture frame
  if (capture_frame()) {

    // 2. create a copy of the captured frame for the draw frame
    drawFrame = capturedFrame.clone();

    // 3. detect all frontal faces
    if (p_detectOnFrameCounter < _detectOnFrame - 1) {
      p_detectOnFrameCounter++;
    } else {
      p_faceClassifier.detect_objects_multiscale(drawFrame, detectedFaces);
      p_detectOnFrameCounter = 0;
    }

    // 4. find largest face (to track)
    largestFaceRect = cv::Rect(0, 0, 0, 0);
    for (size_t i = 0; i < detectedFaces.size(); i++) {
        if (detectedFaces[i].size().width * detectedFaces[i].size().height > largestFaceRect.size().width * largestFaceRect.size().height)
            largestFaceRect = detectedFaces[i];
    }

    // 5. calculate arrow vector: center of screen to center of largest face
    centerScreen = calculate_center(screenDimensions);
    largestFacePoint = calculate_center(largestFaceRect);

    if (detectedFaces.size() > 0) {
        arrow = largestFacePoint - centerScreen;
    } else {
        arrow.x = 0;
        arrow.y = 0;
    }

    // 6. perform face recognition using Recognizer class
    double l_confidence = 0, l_confidenceThreshold = 4000;
    int l_label = p_userRecognizer.eigenface_recognition(drawFrame, l_confidence, l_confidenceThreshold);
    predictedFaceUserName = p_userRecognizer.get_username(l_label);

    return true;

  } else {

    perror("Captured Empty Video Frame!");
    return false;

  }

}

void OdroidVision::draw_detected_faces(cv::Scalar _colour, int _thickness, int _lineType) {

  if (!drawFrame.empty())
    p_faceClassifier.draw_detected_objects(drawFrame, _colour, _thickness, _lineType);

}

void OdroidVision::draw_arrow(cv::Scalar _colour, int _thickness) {

  if (!drawFrame.empty())
    cv::arrowedLine(drawFrame, centerScreen, centerScreen + arrow, _colour, _thickness);

}

void OdroidVision::draw_predictedFaceLabel(int _fontFace, double _fontScale, cv::Scalar _colour, int _thickness, int _lineType, bool _bottomLeftOrigin) {

  if (!drawFrame.empty())
    cv::putText(drawFrame, predictedFaceUserName, cv::Point2f(largestFaceRect.x, largestFaceRect.y), _fontFace, _fontScale, _colour, _thickness, _lineType, _bottomLeftOrigin);

}

void OdroidVision::send_videoFrame(cv::Mat _sendFrame) {

  // Send uncompressed video
  // p_server.send_frame(_sendFrame);

  // Send compressed video
  p_server.send_compressed_frame(_sendFrame);

}

void OdroidVision::display_capturedFrame(std::string _title, int _delay) {

  cv::namedWindow(_title, cv::WINDOW_AUTOSIZE);
  cv::imshow(_title, capturedFrame);
  cv::waitKey(_delay);

}

void OdroidVision::display_drawFrame(std::string _title, int _delay) {

  cv::namedWindow(_title, cv::WINDOW_AUTOSIZE);
  cv::imshow(_title, drawFrame);
  cv::waitKey(_delay);

}

/* Private Methods */

bool OdroidVision::capture_frame(void) {

    bool l_cap = false;

    // Capture Raw Image Frame
    p_camera >> capturedFrame; // capture single image frame

    if (!capturedFrame.empty()) {

        // Record dimensions of screen (the field of view)
        screenDimensions = cv::Rect(0, 0, capturedFrame.size().width, capturedFrame.size().height);

        // Set capture to true
        l_cap = true;
    }

    return l_cap; // return capture status
}

cv::Point OdroidVision::calculate_center(cv::Rect _rectangle)
{
    cv::Point l_center;
    l_center.x = _rectangle.x + _rectangle.width*0.5;
    l_center.y = _rectangle.y + _rectangle.height*0.5;
    return l_center;
}
