/***
  Author:       Alexander Fernandes
  Created:      February 6, 2018
  Last updated: March 13, 2018
 ***/


#include "OdroidStreamer/Recognizer.hpp"

Recognizer::Recognizer(const std::string _userDir, const std::string _eigenFaceXml)   {
  // variables
  p_totalUsers = 0;
  p_iterUser = 0;

  // load EigenFace
  p_eigenface = cv::face::EigenFaceRecognizer::create();
  p_eigenface->read(_eigenFaceXml);

  // load cascades
  cv::CascadeClassifier l_userClassifer;
  struct dirent *l_directory;
  DIR *l_dir = opendir(_userDir.c_str());
  if (l_dir == NULL) {
    std::string l_emsg = "'" + _userDir + "' directory does not exist!";
    perror(l_emsg.c_str());
  }
  l_directory = readdir(l_dir);

  // get list of users
  while (l_directory != NULL) {

    if (l_directory->d_type == DT_DIR) {
      p_userList.push_back(l_directory->d_name);

      // exclude directories
      if (p_userList[p_totalUsers] == "." || p_userList[p_totalUsers] == "..") {

        p_userList.pop_back();

      } else {

        if (!l_userClassifer.load(_userDir + l_directory->d_name + "/cascade.xml")) {
          std::string l_emsg = "Error loading " + _userDir + l_directory->d_name + "/cascade.xml";
          perror(l_emsg.c_str());
        }

        p_userClassifierList.push_back(l_userClassifer);
        p_totalUsers++;

      }

    }

    l_directory = readdir(l_dir);

  }

  closedir(l_dir);

  // OpenCL (use gpu)
  cv::ocl::setUseOpenCL(true);

}

int Recognizer::iterative_recognition(cv::Mat _face) {

  if (_face.empty())
    return -1;

  cv::UMat l_gpuFrame;
  _face.copyTo(l_gpuFrame);

  std::vector<cv::Rect> l_objects;
  cv::cvtColor( l_gpuFrame, l_gpuFrame, cv::COLOR_BGR2GRAY );
  cv::equalizeHist( l_gpuFrame, l_gpuFrame );

  // Detect cascade restricted to minimum detection 75% of cropped face
  p_userClassifierList[p_iterUser].detectMultiScale(
    l_gpuFrame, // input frame
    l_objects, // save detected faces into objects
    1.1, // scaleFactor
    2, // minNeighbors
    0, // flags
    cv::Size(l_gpuFrame.rows * 75, l_gpuFrame.cols * 75) // minSize
  );

  p_iterUser++;
  if (p_iterUser >= p_totalUsers)
    p_iterUser = 0;

  if (l_objects.empty())
    return -1;

  return p_iterUser;

}

int Recognizer::eigenface_recognition(cv::Mat _face, double &_confidence, double _confidenceThreshold) {

  if (_face.empty())
    return -1;

  cv::UMat l_gpuFrame;
  _face.copyTo(l_gpuFrame);
  cv::resize(l_gpuFrame, l_gpuFrame, cv::Size(64, 64));

  std::vector<cv::Rect> l_objects;
  cv::cvtColor( l_gpuFrame, l_gpuFrame, cv::COLOR_BGR2GRAY );
  cv::equalizeHist( l_gpuFrame, l_gpuFrame );

  int l_label = -1;

  p_eigenface->predict(l_gpuFrame, l_label, _confidence);

  if (_confidence > _confidenceThreshold)
    return -1;
  else
    return l_label;

  // Detect cascade restricted to minimum detection 75% of cropped face
  p_userClassifierList[l_label].detectMultiScale(
    l_gpuFrame, // input frame
    l_objects, // save detected faces into objects
    1.1, // scaleFactor
    2, // minNeighbors
    0, // flags
    cv::Size(l_gpuFrame.rows * 75, l_gpuFrame.cols * 75) // minSize
  );

  if (l_objects.empty())
    return -1;

  return l_label;

}

std::string Recognizer::get_username(int _index) {

  if (_index != -1)
		return p_userList[_index];
	else
		return "Unrecognized";

}
