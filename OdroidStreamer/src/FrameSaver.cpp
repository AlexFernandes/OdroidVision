/***
  Author:       Alexander Fernandes
  Created:      March 8, 2018
  Last updated: March 13, 2018
 ***/

#include "OdroidStreamer/FrameSaver.hpp"

/* Constructor */

FrameSaver::FrameSaver(std::string _cropCascadeFile, std::string _dataDir)
  : // Pass arguments to the private variables:
  p_cropCascade(_cropCascadeFile),
  p_dataDir(_dataDir)
{
}

/* Public Methods */

void FrameSaver::save_frame(cv::Mat _frame, std::string _folderName, std::string _fileName) {

  std::string l_sfolderName = p_dataDir + _folderName;

  // If no directory make one
  char l_cfolderName[100];
  sprintf(l_cfolderName, "%s", l_sfolderName.c_str());
  mkdir(l_cfolderName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  cv::imwrite(l_sfolderName + _fileName, _frame);

}

void FrameSaver::save_largest_crop_frame(cv::Mat _frame, std::string _folderName, std::string _fileName) {

  if (p_cropCascade.check_load_error()) {

    std::vector <cv::Rect> l_objects;
    cv::Rect l_largestObj = cv::Rect(0, 0, 0, 0);
    p_cropCascade.detect_objects_multiscale(_frame, l_objects);

    for (size_t i = 0; i < l_objects.size(); i++) {
        if (l_objects[i].size().width * l_objects[i].size().height > l_largestObj.size().width * l_largestObj.size().height)
            l_largestObj = l_objects[i];
    }

    save_frame(_frame(l_largestObj), _folderName, _fileName);

  }

}
