/***
  Author:       Alexander Fernandes
  Created:      March 5, 2018
  Last updated: March 13, 2018
 ***/

#include "OdroidStreamer/CascadeDetector.hpp"

/* Constructor */

CascadeDetector::CascadeDetector(std::string _classifierFile)
  : // Pass arguments to the private variables:
  p_classifier(_classifierFile)
{

  if(!p_classifier.load(_classifierFile)) {

    std::string l_errmsg = "Error loading Cascade Classifier: " + _classifierFile;
    perror(l_errmsg.c_str());
    p_loadError = true;
  } else {
    p_loadError = false;
  }

  // OpenCL (use gpu)
  cv::ocl::setUseOpenCL(true);

}

/* Public Methods */
void CascadeDetector::detect_objects_multiscale(cv::Mat _inputFrame, std::vector <cv::Rect> &_objects, double _scaleFactor, int _minNeighbours, int _flags, cv::Size _minSize, cv::Size _maxSize) {

  if (!_inputFrame.empty()) {

    if (!p_loadError) { // if CascadeClassifier loaded properly

      // Copy to GPU
      cv::UMat gpuFrame;
      _inputFrame.copyTo(gpuFrame);

      // Convert to grayscale and equalize histogram
      cv::cvtColor( gpuFrame, gpuFrame, cv::COLOR_BGR2GRAY );
      cv::equalizeHist( gpuFrame, gpuFrame );

      // perform cascade classifier detection
      p_classifier.detectMultiScale( gpuFrame, _objects, _scaleFactor, _minNeighbours, _flags, _minSize, _maxSize);

      // store copy of objects variable into private variable for draw_detected_objects() to use
      p_objects = _objects;

    }

  } else {

    perror("Input frame empty! Cannot perform cascade classifier detection!");

  }

}

void CascadeDetector::draw_detected_objects(cv::Mat _drawFrame, cv::Scalar _colour, int _thickness, int _lineType, int _shift) {

  for (size_t i = 0; i < p_objects.size(); i++) {

    cv::rectangle(_drawFrame, p_objects[i], _colour, _thickness, _lineType, _shift);

  }

}

bool CascadeDetector::check_load_error() {
  return p_loadError;
}
