/***
  Author:       Alexander Fernandes
  Created:      March 5, 2018
  Last updated: March 21, 2018

  Description:
    Example use of OdroidVision API project
 ***/

// C++ Core libraries

// Project Headers
#include "OdroidStreamer/OdroidVision.hpp"

using namespace std;

int main(int argc, char** argv) {

  int cameraIndex = 0;
  int detectOnFrame = 3;

  cout << "Enter camera index: ";
  cin >> cameraIndex;
  OdroidVision vision("./data/cascades/haarcascade_frontalface_default.xml", cameraIndex);

  // Below are all the public variables of the OdroidVision class:
  std::vector<cv::Rect> detectedFaces;    // list of all detected frontal faces
  std::string           predictedFace;    // name of user with highest predicted face
  cv::Mat               capturedFrame;    // image frame captured by camera (this image is never modified)
  cv::Mat               drawFrame;        // image frame used to draw on
  cv::Rect              screenDimensions; // screen dimensions rectangle
  cv::Rect              largestFaceRect;  // location for largest face as a rectangle
  cv::Point             centerScreen;     // center of screen
  cv::Point             largestFacePoint; // center point of the largest face
  cv::Point             arrow;            // center of screen to center of largest face detected

  while(vision.update(detectOnFrame)) { // 1. Call vision.update() first to update all image processes

    // 2. Call these in either order if you want to draw on top of the image
    vision.draw_detected_faces();
    vision.draw_arrow();
    vision.draw_predictedFaceLabel();

    // Do any processes you want with the public variables:
    // detectedFaces = vision.detectedFaces;
    // predictedFace = vision.predictedFaceUserName;
    // capturedFrame = vision.capturedFrame;
    // drawFrame = vision.drawFrame;
    // screenDimensions = vision.screenDimensions;
    // largestFaceRect = vision.largestFaceRect;
    // centerScreen = vision.centerScreen;
    // largestFacePoint = vision.largestFacePoint;
    // arrow = vision.arrow;

    // 3. Call display function to show either capturedFrame or drawFrame or both!
    // vision.display_capturedFrame("Raw Video");
    // vision.display_drawFrame("Face Tracking");

    // 4. Send any frame over TCP/IP connection to the listening client
    vision.send_videoFrame(vision.capturedFrame); // send raw video feed
    // vision.send_videoFrame(vision.drawFrame); // send draw video feed

  }

}
