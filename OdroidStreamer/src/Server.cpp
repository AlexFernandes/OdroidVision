/***
  Author:       Alexander Fernandes
  Created:      March 10, 2018
  Last updated: March 20, 2018
 ***/

#include "OdroidStreamer/Server.hpp"

/* Constructor */

Server::Server(int _port)
{

  // Setup socket
  p_addressLength = sizeof(struct sockaddr_in);
  p_localSocket = socket(AF_INET, SOCK_STREAM, 0);
  if (p_localSocket == -1)
    perror("Server socket call failed!");

  // Initialize socket address
  p_localAddress.sin_family = AF_INET;
  p_localAddress.sin_addr.s_addr = INADDR_ANY;
  p_localAddress.sin_port = htons( _port );

  // Bind to server socket
  if( bind(p_localSocket,(struct sockaddr *)&p_localAddress , sizeof(p_localAddress)) < 0) {
    perror("Can't bind() socket");
    exit(1);
  }

  p_clientConnected = false;

}

Server::~Server() {
  if (p_clientConnected)
    close(p_clientSocket);
}

/* Public Methods */

void Server::connect(unsigned short _imageWidth, unsigned short _imageHeight, int _backlog) {

  // Listen for connection to a client
  listen(p_localSocket, _backlog);
  std::cout << "Listening for any client." << std::endl;

  while (1) {

    // Attempt connection
    p_clientSocket = accept(p_localSocket, (struct sockaddr *)&p_clientAddress, (socklen_t *)&p_addressLength);

    if (p_clientSocket < 0) {
      perror("Client accept failed!");
    } else {
      // Connected!
      std::cout << "Connected to client!" << std::endl;

      // Send video dimensions
      std::cout << "Sending Video Width: " << _imageWidth << std::endl;
      std::cout << "Sending video Height: " << _imageHeight << std::endl;

      send(p_clientSocket, (const char*)&_imageWidth, sizeof(_imageWidth), 0);
      send(p_clientSocket, (const char*)&_imageHeight, sizeof(_imageHeight), 0);

      p_clientConnected = true;
      break;
    }

  }

}

ssize_t Server::send_frame(cv::Mat _frame) {

  // Convert to Grayscale Image (compress image to 3 times smaller)
  // cvtColor( _frame, _frame, CV_BGR2GRAY );

  return send(p_clientSocket, _frame.data, _frame.total() * _frame.elemSize(), 0);

}

void Server::send_compressed_frame(cv::Mat _frame) {

  std::vector<uchar>  l_bufVector;
  unsigned int        l_bufSize;

  if (cv::imencode(".jpg", _frame, l_bufVector)) {

    l_bufSize = l_bufVector.size();
    uchar l_buf[l_bufSize];
    std::copy(l_bufVector.begin(), l_bufVector.end(), l_buf);
    send(p_clientSocket, &l_bufSize, sizeof(l_bufSize), 0);
    send(p_clientSocket, l_buf, l_bufSize, 0);

  }

}
